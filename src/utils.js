import { local_get, local_set } from './localStorage'

export function getKeypairForId(id) {
    const res = local_get(`id_${id}`)
    return res ? JSON.parse(res): undefined
}

export function setKeypairForId(id, obj) {
    return local_set(`id_${id}`, JSON.stringify(obj))
}

export function checkKeypairExistForId(id) {
    const res = !!getKeypairForId(id)
    if (res) console.log(id + 'exists')
    return res
}
