import { bytesToBase64 } from './base64'
import { sign } from 'tweetnacl'
import Axios from 'axios'
import { backendHost } from './config'
import { getKeypairForId, setKeypairForId, checkKeypairExistForId } from './utils'

function genKeyPair(id) {
    const key = sign.keyPair()
    const pub = bytesToBase64(key.publicKey)
    const pri = bytesToBase64(key.secretKey)
    const obj = { pub, pri }

    setKeypairForId(id, obj)
    
    console.log(getKeypairForId(id))
    
    return obj
}

export async function genKeyPairAndSend(id) {

    if (typeof id !== 'string') {
        throw {
            msg: '"id" should be string.'
        }
    }

    const { pub } = genKeyPair(id)

    try {
        const res = await Axios.post(backendHost + '/create', { studentID: parseInt(id), publicKey: pub })
        console.log(JSON.stringify(res))
    } catch (error) {
        console.error(JSON.stringify(error.error))
        throw error
    }
}
