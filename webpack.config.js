require('dotenv').config()
const Dotenv = require('dotenv-webpack');

module.exports = {
    entry: './src/index.js',
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
        ]
    },
    resolve: {
        extensions: ['*', '.js']
    },
    output: {
        path: __dirname + '/dist',
        publicPath: '/',
        filename: 'grader-extension-frontend.js',
        library: process.env.GENERAL_PREFIX
    },
    devServer: {
        contentBase: './dist'
    },
    plugins: [
        new Dotenv()
    ]
};
